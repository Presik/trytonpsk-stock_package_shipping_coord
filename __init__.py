# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import shipment
from . import configuration


def register():
    Pool.register(
        shipment.ShipmentOut,
        configuration.Configuration,
        module='stock_package_shipping_coord', type_='model')

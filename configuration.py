# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import PoolMeta
from trytond.model import fields


class Configuration(metaclass=PoolMeta):
    __name__ = 'stock.configuration'
    client_id_carrier_ws = fields.Char('Client ID Carrier WS ')
    user_carrier_ws = fields.Char('User Carrier WS ')
    password_carrier_ws = fields.Char('Password Carrier WS')

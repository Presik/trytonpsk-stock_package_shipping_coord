# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from .test_stock_package_shipoing_coord import suite

__all__ = ['suite']

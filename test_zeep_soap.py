#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  test_zeep.py
#
#  Copyright 2018 Oscar Alvarez <oscar@oscar-pc>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import hashlib
from zeep import Client

client_id = '29326'
user = 'forestservices.ws'
password = 'Fjiu876!?o'

sha_signature = hashlib.sha256(password.encode()).hexdigest()

wsdl = 'http://sandbox.coordinadora.com/agw/ws/guias/1.6/server.php?wsdl'

codigo_remision = ''
fecha = '2018-11-06'
id_cliente = 29326
direccion = 'Cra 31 N 50 19'
id_remitente = 0
nombre_remitente = 'Remite Demo Prueba'
phone = '6435479'
ciudad_remitente = '05001000'
nit_destinatario = '1385354'
div_destinatario = 'Santander'
nombre_destinatario = 'Brad Pitt'
direccion_destinatario = 'Berverly hills 90210'
ciudad_destinatario = '05001000'
telefono_destinatario = '6945858'
valor_declarado = '56000'
contenido = 'Paquete de uniformes'
referencia = 'REM013874'
codigo_cuenta = 1
codigo_producto = 0
nivel_servicio = 1
product_name = 'UNIFORME'

client = Client(wsdl=wsdl)

# https://python-zeep.readthedocs.io/en/master/datastructures.html

arrayTypeGuiaDetalle = client.get_type('ns0:ArrayOfAgw_typeGuiaDetalle')
typeGuiaDetalle = client.get_type('ns0:Agw_typeGuiaDetalle')

detalle = typeGuiaDetalle(
    ubl=0,
    alto=10,
    largo=10,
    ancho=10,
    peso=1,
    unidades=1,
    referencia=product_name,
    nombre_empaque='Caja'
)

detalles = arrayTypeGuiaDetalle([detalle])

res = client.service.Guias_generarGuia({
    'usuario': user,
    'clave': sha_signature,
    'id_cliente': id_cliente,
    'id_remitente': id_remitente,
    'codigo_remision': codigo_remision,
    'nombre_remitente': nombre_remitente,
    'direccion_remitente': direccion,
    'telefono_remitente': phone,
    'ciudad_remitente': ciudad_remitente,
    'fecha': fecha,
    'nit_destinatario': nit_destinatario,
    'div_destinatario': div_destinatario,
    'nombre_destinatario': nombre_destinatario,
    'direccion_destinatario': direccion_destinatario,
    'ciudad_destinatario': ciudad_destinatario,
    'telefono_destinatario': telefono_destinatario,
    'valor_declarado': valor_declarado,
    'codigo_producto': codigo_producto,
    'contenido': contenido,
    'referencia': referencia,
    'codigo_cuenta': codigo_cuenta,
    'cuenta_contable': '',
    'centro_costos': '',
    'recaudos': [],
    'nivel_servicio': nivel_servicio,
    'linea': '',
    'observaciones': '',
    'estado': 'IMPRESO',
    'detalle': detalles,
    'margen_izquierdo': 0,
    'margen_superior': 0,
    'usuario_vmi': '',
    'formato_impresion': '',
    'atributo1_nombre': '',
    'atributo1_valor': '',
    'notificaciones': [],
    'atributos_retorno': [],
    'nro_doc_radicados': '',
    'nro_sobre': ''
})

print('id_remision:', res['id_remision'])
print('codigo_remision:', res['codigo_remision'])
print('url_terceros:', res['url_terceros'])
